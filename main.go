# health as dcos by @fajarhide
package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"strings"
	"time"
)

func main() {
	for {
		checking()
	}
}

func checking() {
	fHandle, err := os.Open("url")
	if err != nil {
		fmt.Println(err)
	}
	defer fHandle.Close()
	fScanner := bufio.NewScanner(fHandle)
	for fScanner.Scan() {
		urls := fScanner.Text()
		resp, err := http.Get(urls)
		if err != nil {
			log.Fatal(err)
		}
		if resp.StatusCode == 503 || resp.StatusCode == 502 {
			request_url := "https://api.telegram.org/bot{{TOKEN}}/sendMessage?chat_id={{ID}}"
			client := &http.Client{}
			fmt.Println(urls, "=>", resp.StatusCode)
			u, err := url.Parse(urls)
			if err != nil {
				log.Fatal(err)
			}
			result := u.Path
			parse := strings.Split(result, "/")
			app := parse[1]
			out, err := exec.Command("/bin/bash", "dcos", app).Output()
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%s\n", out)
			values := map[string]string{"text": resp.Status + "\n" + urls + "\n" + "Continue restarted service : " + app}
			jsonStr, err := json.Marshal(values)
			if err != nil {
				fmt.Println(err)
			}
			req, err := http.NewRequest("POST", request_url, bytes.NewBuffer(jsonStr))
			if err != nil {
				fmt.Println(err)
			}
			req.Header.Set("Content-Type", "application/json")
			client.Do(req)
		}
		time.Sleep(time.Second * 5)
	}
}
